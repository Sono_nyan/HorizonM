#include <platform.h>

#include <stdio.h>
#include <string.h>

SDL_Window* win = 0;
SDL_Renderer* rendertop = 0;
SDL_Texture* tex[3] = {0, 0, 0};
SDL_Surface* img[3] = {0, 0, 0};

SDL_Joystick* joy = 0;
int joyid = -1;

static u32 kdown;
static u32 kheld;
static u32 kup;
static u32 kold;
static u32 ktick;
//static u32 ktickold;
static circlePosition cpos;
static circlePosition dpos;
static touchPosition touch;

SDL_Surface* mksurface(int width, int height, int bsiz, int pixfmt);

void __attribute__((weak)) SDL_FixRender();


int btnmap_kbd[PAD_MAX] =
{
    SDL_SCANCODE_A,
    SDL_SCANCODE_B,
    SDL_SCANCODE_P,
    SDL_SCANCODE_L,
    SDL_SCANCODE_RIGHT,
    SDL_SCANCODE_LEFT,
    SDL_SCANCODE_UP,
    SDL_SCANCODE_DOWN,
    SDL_SCANCODE_E,
    SDL_SCANCODE_Q,
    SDL_SCANCODE_X,
    SDL_SCANCODE_Z, //fuck QWERTY
    -1, //DUMMY12
    -1, //DUMMY13
    -1, //ZL
    -1, //ZR
    -1  //TOUCH
};

int btnmap_pad[PAD_MAX] =
{
    1,
    2,
    8,
    9,
    -1,
    -1,
    -1,
    -1,
    7,
    6,
    0,
    3,
    -1,
    -1,
    4,
    5,
    -1
};


Result gfxw_init()
{
    SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS,"1");
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0");
    SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1");
#ifdef _WIN32
    SDL_SetHint(SDL_HINT_RENDER_DRIVER, "opengl");
#endif
    
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK /*| SDL_INIT_AUDIO*/) < 0)
    {
        printf("Failed to init SDL: %s\n", SDL_GetError());
        return -1;
    }
    
    win = SDL_CreateWindow("HorizonScreen " BUILDTIME, 1,1/*SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED*/, 720, 480, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE/* | SDL_WINDOW_OPENGL*/);
    if(!win)
    {
        printf("Can't create window: %s\n", SDL_GetError());
        return -2;
    }
    
    rendertop = SDL_CreateRenderer(win, -1, 0/*SDL_RENDER_PRESENTVSYNC*/);
    
    SDL_RenderSetLogicalSize(rendertop, 720, 480);
    
    SDL_RendererInfo renderinfo;
    if(SDL_GetRendererInfo(rendertop, &renderinfo))
    {
        printf("Failed to get renderer info: %s\n", SDL_GetError());
    }
    else
    {
        printf("Adapter: %s\n", renderinfo.name ? renderinfo.name : "(null)");
        printf("MaxTex*: %ix%i\n", renderinfo.max_texture_width, renderinfo.max_texture_height);
    }
    
    
    img[0] = mksurface(240, 400, 3, 1);
    img[2] = mksurface(240, 400, 3, 1);
    img[1] = mksurface(240, 320, 3, 1);
    
    return 0;
}

void gfxw_exit()
{
    if(joy) SDL_JoystickClose(joy);
    
    if(tex[0]) SDL_DestroyTexture(tex[0]);
    if(tex[2]) SDL_DestroyTexture(tex[2]);
    if(tex[1]) SDL_DestroyTexture(tex[1]);
    
    if(img[0]) SDL_FreeSurface(img[0]);
    if(img[2]) SDL_FreeSurface(img[2]);
    if(img[1]) SDL_FreeSurface(img[1]);
      
    if(win) SDL_DestroyWindow(win);
    SDL_Quit();
}



void hidScanInput()
{
    //SDL_JoystickUpdate();
    kdown = (~kold) & kheld;
    kdown |= ktick;
    kup = kold & (~kheld);
    //kup |= ktickold;
    kup |= ktick;
    kold = kheld;
    //ktickold = ktick;
    ktick = 0;
}

u32 hidKeysDown()
{
    return kdown;
}

u32 hidKeysHeld()
{
    return kheld;
}

u32 hidKeysUp()
{
    return kup;
}

void hidTouchRead(touchPosition* pos)
{
    if(pos) *pos = touch;
}

void hidCircleRead(circlePosition* pos)
{
    if(pos) *pos = cpos;
}

void irrstCStickRead(circlePosition* pos)
{
    if(pos) *pos = dpos;
}



int PumpEvent()
{
    SDL_Event evt;
    
    int i;
    int j;
    
    while(SDL_PollEvent(&evt))
    {
        switch(evt.type)
        {
            case SDL_QUIT:
            case SDL_APP_TERMINATING:
                return 0;
                
            case SDL_KEYDOWN:
                //printf("Key down: %i\n", evt.key.keysym.scancode);
                j = evt.key.keysym.scancode;
                for(i = 0; i != PAD_SPECIAL; i++)
                {
                    if(btnmap_kbd[i] == j)
                    {
                        kheld |= (1 << i);
                    }
                }
                
                break;
                
            case SDL_KEYUP:
                //printf("Key up: %i\n", evt.key.keysym.scancode);
                j = evt.key.keysym.scancode;
                for(i = 0; i != PAD_SPECIAL; i++)
                {
                    if(btnmap_kbd[i] == j)
                    {
                        kheld &= ~(1 << i);
                    }
                }
                
                break;
                
            case SDL_JOYDEVICEADDED:
                if(joy) SDL_JoystickClose(joy);
                
                joyid = -1;
                
                joy = SDL_JoystickOpen(evt.jdevice.which);
                if(!joy) break;
                
                joyid = SDL_JoystickInstanceID(joy);
                
                printf("Joystick connected: %s\n", SDL_JoystickName(joy));
                
                SDL_JoystickEventState(1);
                
                break;
                
            case SDL_JOYDEVICEREMOVED:
                if(!joy || evt.jdevice.which != joyid) break;
                
                puts("Joystick disconnected");
                
                SDL_JoystickClose(joy);
                joy = 0;
                joyid = -1;
                
                break;
                
            case SDL_JOYBUTTONDOWN:
                j = evt.jbutton.button;
                printf("joy %i\n", j);
                for(i = 0; i != PAD_SPECIAL; i++)
                {
                    if(btnmap_pad[i] == j)
                    {
                        kheld |= (1 << i);
                    }
                }
                
                break;
                
            case SDL_JOYBUTTONUP:
                j = evt.jbutton.button;
                for(i = 0; i != PAD_SPECIAL; i++)
                {
                    if(btnmap_pad[i] == j)
                    {
                        kheld &= ~(1 << i);
                    }
                }
                
                break;
                
            case SDL_MOUSEMOTION:
                //printf("Mouse move @ %i;%i\n", evt.motion.x, evt.motion.y);
                touch.px = evt.motion.x;
                touch.py = evt.motion.y;
                
                if(touch.px & 0x8000) touch.px = 0;
                //else if(touch.px >= rec.w) touch.px = rec.w - 1;
                if(touch.py & 0x8000) touch.py = 0;
                //else if(touch.py >= rec.h) touch.py = rec.h - 1;
                
                break;
                
            case SDL_MOUSEBUTTONDOWN:
                kheld |= KEY_TOUCH;
                
                break;
                
            case SDL_MOUSEBUTTONUP:
                kheld &= ~KEY_TOUCH;
                
                break;
                
            case SDL_MOUSEWHEEL:
                if(evt.wheel.y == 1) ktick |= KEY_DUP;
                else if(evt.wheel.y == -1) ktick |= KEY_DDOWN;
                
                break;
                
            case SDL_WINDOWEVENT:
                switch(evt.window.event)
                {
                    case SDL_WINDOWEVENT_MINIMIZED:
                        /*while(SDL_WaitEvent(&evt))
                        {
                            switch(evt.type)
                            {
                                case SDL_QUIT:
                                case SDL_APP_TERMINATING:
                                    return 0;   
                            }
                            if(evt.type != SDL_WINDOWEVENT) continue;
                            if(evt.window.event == SDL_WINDOWEVENT_RESTORED) break;
                        }*/
                        
                        break;
                        
                    //case SDL_WINDOWEVENT_RESIZED:
                    //    puts("Window resized");                        
                    //    break;
                    
                    case SDL_WINDOWEVENT_RESIZED:
                    case SDL_WINDOWEVENT_RESTORED:
                        if(SDL_FixRender) SDL_FixRender();
                        break;
                    
                    default:
                        //printf("Window event: %i\n", evt.window.event);
                        
                        break;
                }
                
                break;
                
            default:
                //printf("SDL Event: %i\n", evt.type);
                
                break;
        }
    }
    
    if(joy)
    {
        kheld &= ~\
        (
            KEY_DUP | KEY_DDOWN | KEY_DLEFT | KEY_DRIGHT |\
            KEY_CPAD_UP | KEY_CPAD_DOWN | KEY_CPAD_LEFT | KEY_CPAD_RIGHT |\
            KEY_CSTICK_UP | KEY_CSTICK_DOWN | KEY_CSTICK_LEFT | KEY_CSTICK_RIGHT \
        );
        
        int hat = SDL_JoystickGetHat(joy, 0);
        if(hat)
        {
            if(hat & SDL_HAT_UP) kheld |= KEY_DUP;
            if(hat & SDL_HAT_DOWN) kheld |= KEY_DDOWN;
            if(hat & SDL_HAT_LEFT) kheld |= KEY_DLEFT;
            if(hat & SDL_HAT_RIGHT) kheld |= KEY_DRIGHT;
        }
        
        cpos.dx = SDL_JoystickGetAxis(joy, 0) / 250;
        cpos.dy = SDL_JoystickGetAxis(joy, 1) / 250;
        
        if(cpos.dx) kheld |= cpos.dx > 0 ? KEY_CPAD_RIGHT : KEY_CPAD_LEFT;
        if(cpos.dy) kheld |= cpos.dy > 0 ? KEY_CPAD_DOWN : KEY_CPAD_UP;
        
#ifdef WIN32
        dpos.dx = SDL_JoystickGetAxis(joy, 4) / 250;
        dpos.dy = SDL_JoystickGetAxis(joy, 3) / 250;
#else
		dpos.dx = SDL_JoystickGetAxis(joy, 3) / 250;
        dpos.dy = SDL_JoystickGetAxis(joy, 2) / 250;
#endif
        
        if(dpos.dx) kheld |= dpos.dx > 0 ? KEY_CSTICK_RIGHT : KEY_CSTICK_LEFT;
        if(dpos.dy) kheld |= dpos.dy > 0 ? KEY_CSTICK_DOWN : KEY_CSTICK_UP;
    }
    
    return 1;
}
