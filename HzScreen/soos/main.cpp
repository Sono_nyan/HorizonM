/*
    HorizonScreen - remote client for HorizonModule
    Copyright (C) 2017 MarcusD (https://github.com/MarcuzD)
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _WIN32_WINNT 0x0501
#include <platform.hpp>

#include "ChaiScript.hpp"

extern "C"
{
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <setjmp.h>
#include <sys/stat.h>
#include <sys/types.h>
#if __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif
#include <errno.h>
#include <stdarg.h>
#include <fcntl.h>
#ifndef WIN32
#include <arpa/inet.h>
#include <netdb.h>
typedef int SOCKET;
#endif
#include <poll.h>

#include "inet_pton.h"

#include "tga/targa.h"
#include <turbojpeg.h>
//#include "lz4/lz4.h"

#include "font.h"

int _kbhit(void);
}

#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
typedef int socklen_t;
#define POLLIN 2
#define POLLERR 1
#endif

#include <exception>
#include <iostream>
#include <istream>
#include <ostream>

#include "ini.hpp"

using ::abs;
using namespace std;
using MM::Ini;


#define STRINGIFY_WAT(wat) #wat
#define STRINGIFY(wat) STRINGIFY_WAT(wat)

#define CONCAT_WAT(wat,meh) wat##meh
#define CONCAT(wat,meh) CONCAT_WAT(wat,meh)


#define FPSNO 5
float fps = 0.0F;
u32 fpsticks[FPSNO];
u32 fpstick = 0;
int fpscurrwrite = 0;
int fpsoldwrite = 0;

#define BPSNO 10
float bps = 0.0F;
float bpsticks[BPSNO];
u32 bpstick = 0;
int bpscurrwrite = 0;
int bpsoldwrite = 0;

char printbuf[0x100];

int dbg = 0;

#define fail(wut) { wut; goto killswitch; }

#define err(wut) { printf(#wut " fail (line #%03i): (%i) %s\n", __LINE__, errno, strerror(errno)); }
#define errtga(wut) { printf(#wut " fail (line #%03i): (%i) %s\n", __LINE__, res, tga_error(res)); }
#define errjpeg() { printf("JPEG fail (line #%03i): %s\n", __LINE__, tjGetErrorStr()); }

#ifdef WIN32
#define wsaerr(func)\
{\
    wchar_t *s = NULL;\
    FormatMessageW\
    (\
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,\
        NULL, WSAGetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&s, 0, NULL\
    );\
    printf(#func " fail (line #%03i): (%i) %S\n", __LINE__, WSAGetLastError(), s);\
    LocalFree(s);\
}
#else
#define wsaerr err
#endif

int pollsock(SOCKET sock, int wat, int timeout = 0)
{
#ifdef WIN32
    fd_set fd;
    fd.fd_count = 1;
    fd.fd_array[0] = sock;
    TIMEVAL t;
    t.tv_sec = timeout / 1000;
    t.tv_usec = (timeout % 1000) * 1e6;
    int ret = select(1, (wat & POLLIN) ? &fd : nullptr , nullptr, (wat & POLLERR) ? &fd : nullptr, &t);
    if(ret == SOCKET_ERROR) return (wat & POLLERR) == POLLERR;
    return ret ? wat : 0;
#else
    struct pollfd pd;
    pd.fd = sock;
    pd.events = wat;
    
    if(poll(&pd, 1, timeout) == 1)
        return pd.revents & wat;
#endif
    return 0;
}

class bufsoc
{
public:
    
    struct packet
    {
        u32 packetid : 8;
        u32 size : 24;
        u8 data[0];
    };
    
    SOCKET sock;
    u8* buf;
    int bufsize;
    int recvsize;
    
    bufsoc(SOCKET sock, int bufsize = 1024 * 1024) : sock(sock)
    {
        this->bufsize = bufsize;
        buf = new u8[bufsize];
        
        recvsize = 0;
    }
    
    ~bufsoc()
    {
        delete[] buf;
    }
    
    int avail()
    {
        return pollsock(sock, POLLIN) == POLLIN;
    }
    
    int hazerr()
    {
        return pollsock(sock, POLLERR) == POLLERR;
    }
    
    int readbuf(int flags = 0)
    {
        u32 hdr = 0;
        int ret = recv(sock, (char*)&hdr, 4, flags);
        if(ret < 0) return -errno;
        if(ret < 4) return -1;
        *(u32*)buf = hdr;
        
        packet* p = pack();
        
        int mustwri = p->size;
        int offs = 4;
        while(mustwri)
        {
            ret = recv(sock, (char*)(buf + offs), mustwri, flags);
            if(ret <= 0) return -errno;
            mustwri -= ret;
            offs += ret;
        }
        
        recvsize = offs;
        return offs;
    }
    
    int wribuf(int flags = 0)
    {
        int mustwri = pack()->size + 4;
        int offs = 0;
        int ret = 0;
        while(mustwri)
        {
            ret = send(sock, (char*)(buf + offs) , mustwri, flags);
            if(ret <= 0) return -errno;
            mustwri -= ret;
            offs += ret;
        }
        
        return offs;
    }
    
    packet* pack()
    {
        return (packet*)buf;
    }
    
    int errformat(char* c, ...)
    {
        int len = 0;
        
        packet* p = pack();
        
        va_list args;
        va_start(args, c);
        len = vsnprintf((char*)(p->data + 1), 256, c, args);
        va_end(args);
        
        if(len < 0)
        {
            puts("wat");
            return -1;
        }
        
        printf("Packet error %i: %s\n", p->packetid, (char*)(p->data + 1));
        
        p->data[0] = p->packetid;
        p->packetid = 1;
        p->size = (len * sizeof(char)) + 2;
        
        return wribuf();
    }
};

extern "C" SDL_Surface* mksurface(int width, int height, int bsiz, int pixfmt)
{
    int rm, gm, bm, am, bs;
    
    switch(pixfmt & 7)
    {
        case 0:
            rm = 0x000000FF;
            gm = 0x0000FF00;
            bm = 0x00FF0000;
            am = 0;//0xFF000000;
            bs = 4;
            break;
        case 2:
            rm = 0xF800;
            gm = 0x07E0;
            bm = 0x001F;
            am = 0;
            bs = 2;
            break;
        case 3:
            rm = 0xF800;
            gm = 0x07C0;
            bm = 0x003E;
            am = 0x0001;
            bs = 2;
            break;
        case 4:
            rm = 0x000F;
            gm = 0x00F0;
            bm = 0x0F00;
            am = 0xF000;
            bs = 2;
            break;
        default:
            rm = 0xFF0000;
            gm = 0x00FF00;
            bm = 0x0000FF;
            am = 0;
            bs = 3;
            break;
    }
    
    printf("Surface: %ix%i %ibpp (%08X %08X %08X %08X)\n", width, height, bs << 3, rm, gm, bm, am);
    SDL_Surface* surf = SDL_CreateRGBSurface(0, width, height, bs << 3, rm, gm, bm, am);
    if(!surf)
    {
        printf("No surface! %s\n", SDL_GetError());
    }
    
    return surf;
}


extern "C"
{
extern SDL_Window* win;
extern SDL_Renderer* rendertop;
extern SDL_Texture* tex[3];
extern SDL_Surface* img[3];

extern SDL_Joystick* joy;
extern int joyid;

extern int btnmap_pad[PAD_MAX];
extern int btnmap_kbd[PAD_MAX];
}


static u32 kDown;
static u32 kHeld;
static u32 kUp;
static circlePosition cpos, dpos;
static touchPosition touch;
static int DInput = 0;

static int infd = 0;

static int port = 6464;
static SOCKET sock = 0;
static struct sockaddr_in sao;
static socklen_t sizeof_sao = sizeof(sao);
static bufsoc* soc = 0;
static bufsoc::packet* p = 0;

static u32* pdata = 0;


static u8 sbuf[256 * 400 * 4 * 3];
static u8 decbuf[256 * 400 * 4];
static int srcfmt[2] = {3, 3};
static int stride[2] = {480, 480};
static int bsiz[2] = {2, 2};
static int ret = 0;

static int jpgq = 0;
static int scre = 0;

static tga_image tga;
static tga_result res;

static tjhandle jdec = nullptr;

static Ini cfg;


void drawtext(const char* wat, int sx, int sy, int color = 0, int scx = 1, int scy = 1, int esx = 0xFFFF, int esy = 0xFFFF)
{
    rendertext(wat, sx, sy, esx, esy, -1, color, scx, scy);
}

void drawstring(const string str, int sx, int sy, int esx = 0xFFFF, int esy = 0xFFFF, int color = 0, int scx = 1, int scy = 1)
{
    rendertext(str.c_str(), sx, sy, esx, esy, str.length(), color, scx, scy);
}

fstream& frewind(fstream& fs)
{
    fs.clear();
    fs.seekg(0);
    
    return fs;
}

void CFGLoad()
{
    fstream fo("HzS.ini", fstream::in);
    
    fo >> cfg;
    
    fo.close();
}

void CFGSave()
{
    fstream fo("HzS.ini", fstream::out);
    
    fo << cfg << endl;
    
    fo.close();
}

extern "C" void SDL_FixRender()
{
    //puts("should be fixing the renderer rn pls kthx");
    
    deinittext();
    
    inittext(rendertop);
}


void destroyconnect()
{
    if(soc)
    {
        delete soc;
        soc = nullptr;
        p = nullptr;
    }
    if(sock)
    {
        
#ifdef WIN32
        shutdown(sock, SD_BOTH);
        closesocket(sock);
#else
        shutdown(sock, SHUT_RDWR);
        close(sock);
#endif
        sock = 0;
    }
}

int setupconnect(const string& addr)
{
    destroyconnect();
    
    if(!inet_pton4(addr.c_str(), (unsigned char*)&sao.sin_addr))
    {
        printf("Malformatted IP address: '%s'\n", addr.c_str());
        return -1;
    }
    
    sao.sin_family = AF_INET;
    sao.sin_port = htons(port);
    
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
    if(sock <= 0) fail(wsaerr(socket));
    
    sizeof_sao = sizeof(sao);
    ret = connect(sock, (sockaddr*)&sao, sizeof_sao);
    if(ret < 0) fail(wsaerr(connect)); 
    
    soc = new bufsoc(sock, 0x200000);
    p = soc->pack();
    
    
    return 0;
    
    killswitch:
    
    destroyconnect();
    return -1;
}

u8 pkt_read8(u32 offs) { return p->data[offs]; }
s8 pkt_reads8(u32 offs) { return (s8)p->data[offs]; }
u16 pkt_read16(u32 offs) { return *(u16*)&p->data[offs]; }
s16 pkt_reads16(u32 offs) { return *(s16*)&p->data[offs]; }
u32 pkt_read32(u32 offs) { return *(u32*)&p->data[offs]; }
s32 pkt_reads32(u32 offs) { return *(s32*)&p->data[offs]; }
u64 pkt_read64(u32 offs) { return *(u64*)&p->data[offs]; }
s64 pkt_reads64(u32 offs) { return *(s64*)&p->data[offs]; }

u8 pkt_readid() { return p->packetid; }
u32 pkt_readsize() { return p->size; }

void pkt_put8(u32 offs, u8 val) { p->data[offs] = val; }
void pkt_puts8(u32 offs, s8 val) { p->data[offs] = (u8)val; }
void pkt_put16(u32 offs, u16 val) { *(u16*)&p->data[offs] = val; }
void pkt_puts16(u32 offs, s16 val) { *(s16*)&p->data[offs] = val; }
void pkt_put32(u32 offs, u32 val) { *(u32*)&p->data[offs] = val; }
void pkt_puts32(u32 offs, s32 val) { *(s32*)&p->data[offs] = val; }
void pkt_put64(u32 offs, u64 val) { *(u64*)&p->data[offs] = val; }
void pkt_puts64(u32 offs, s64 val) { *(s64*)&p->data[offs] = val; }

void pkt_putid(u8 val) { p->packetid = val; }
void pkt_putsize(u32 val) { p->size = val; }

int pkt_out() { return soc->wribuf(); }

int pkt_ldpatch(const string pth)
{
    string concat = "HzSData/patches/" + pth + ".hzp";
    
    FILE* f = fopen(concat.c_str(), "rb");
    if(f <= 0)
    {
        printf("Failed to open patch file '%s': (%i) %s\n", concat.c_str(), errno, strerror(errno));
        return -errno;
    }
    
    u8 buf[0x1008];
    int ret = fread(buf, 1, sizeof(buf), f);
    if(ret < 0)
    {
        printf("Failed to read patch file '%s': (%i) %s\n", concat.c_str(), errno, strerror(errno));
        fclose(f);
        return -errno;
    }
    else if(ret <= 8)
    {
        printf("Invalid patch file '%s': file too small\n", concat.c_str());
        fclose(f);
        return -1;
    }
    else
    {
        fclose(f);
        memcpy(p->data, buf, sizeof(buf));
        p->packetid = 0x81;
        p->size = ret;
    }
    
    return ret - 8;
}


int main(int argc, char** argv)
{
    memset(fpsticks, 0, sizeof(fpsticks));
    
    jdec = tjInitDecompress();
    if(!jdec) {errjpeg(); return 1;}
    
    if(gfxw_init() < 0) { err(gfxw_init); return 1; }
    
#ifdef WIN32
    
    WSADATA socHandle;
    
    ret = WSAStartup(MAKEWORD(2,2), &socHandle);
    if(ret)
    {
        printf("WSAStartup failed: %i\n", ret);
        gfxw_exit();
        return 1;
    }
    
#endif
    
#ifdef WIN32
    //HWND wnd = CreateFile(L"CONIN$", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED | FILE_FLAG_NO_BUFFERING, NULL);
    //infd = _fileno(wnd);
    infd = _fileno(stdin);
    if(!_isatty(infd)) _setmode(infd, _O_BINARY);
#else
    infd = fileno(stdin);
#endif
    
    CFGLoad();
    
    /*do
    {
        struct timeval timeout;
        timeout.tv_sec = 10;
        timeout.tv_usec = 0;
        
        setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));
    }
    while(0);*/
    
    inittext(rendertop);
    
    
    
    #define REG(wat) scr.add(chaiscript::user_type<wat>(), STRINGIFY(wat))
    
    //REG(size_t);
    REG(u8);
    REG(u16);
    REG(u32);
    REG(u64);
    REG(s8);
    REG(s16);
    REG(s32);
    REG(s64);
    
    #ifdef WIN32
    REG(SOCKET);
    #endif
    REG(bufsoc);
    scr.add(chaiscript::user_type<bufsoc::packet>(), "packet");
    REG(Ini);
    scr.add(chaiscript::user_type<Ini::IniSection>(), "IniSection");
    
    #undef REG
    #define REG(cls,wat) scr.add(chaiscript::fun(&cls::wat), STRINGIFY(wat))
    
    REG(bufsoc,readbuf);
    REG(bufsoc,wribuf);
    REG(bufsoc,avail);
    REG(bufsoc,hazerr);
    //REG(bufsoc,errformat);
    
    REG(Ini,Get);
    REG(Ini,Haz);
    REG(Ini,Delet);
    REG(Ini,Reset);
    
    REG(Ini::IniSection,Get);
    REG(Ini::IniSection,GetBinaryString);
    REG(Ini::IniSection,PutBinaryString);
    REG(Ini::IniSection,Haz);
    REG(Ini::IniSection,Delet);
    REG(Ini::IniSection,Reset);
    
    #undef REG
    #define REG(wat) scr.add(chaiscript::var(std::ref(wat)), STRINGIFY(wat))
    
    REG(kDown);
    REG(kHeld);
    REG(kUp);
    
    scr.add(chaiscript::var(std::ref(cpos.dx)), "cpos_x");
    scr.add(chaiscript::var(std::ref(cpos.dy)), "cpos_y");
    scr.add(chaiscript::var(std::ref(dpos.dx)), "dpos_x");
    scr.add(chaiscript::var(std::ref(dpos.dy)), "dpos_y");
    scr.add(chaiscript::var(std::ref(touch.px)), "touchX");
    scr.add(chaiscript::var(std::ref(touch.py)), "touchY");
    
    REG(soc);
    REG(p);
    REG(cfg);
    
    #undef REG
    #define REG(wat) scr.add(chaiscript::fun(&wat), STRINGIFY(wat))
    
    REG(drawstring);
    REG(CFGLoad);
    REG(CFGSave);
    REG(setupconnect);
    REG(destroyconnect);
    
    REG(pkt_read8);
    REG(pkt_reads8);
    REG(pkt_read16);
    REG(pkt_reads16);
    REG(pkt_read32);
    REG(pkt_reads32);
    REG(pkt_read64);
    REG(pkt_reads64);
    REG(pkt_readid);
    REG(pkt_readsize);
    
    REG(pkt_put8);
    REG(pkt_puts8);
    REG(pkt_put16);
    REG(pkt_puts16);
    REG(pkt_put32);
    REG(pkt_puts32);
    REG(pkt_put64);
    REG(pkt_puts64);
    REG(pkt_putid);
    REG(pkt_putsize);
    
    REG(pkt_out);
    REG(pkt_ldpatch);
    
    #undef REG
    
    
    scr.eval_file("HzSData/script/main.hzc");
    
    while(PumpEvent())
    {
        hidScanInput();
        
        kDown = hidKeysDown();
        kHeld = hidKeysHeld();
        kUp = hidKeysUp();
        
        hidCircleRead(&cpos);
        irrstCStickRead(&dpos);
        if(kHeld & KEY_TOUCH) hidTouchRead(&touch);
        
        if((kHeld & (KEY_SELECT | KEY_START)) == (KEY_START | KEY_SELECT))
        {
            if(kDown & KEY_START)
            {
                DInput = !DInput;
                
                if(DInput)
                    scr.eval("Hz_DInput(true);");
                else
                    scr.eval("Hz_DInput(false);");
            }
            
            kHeld &= ~(KEY_START | KEY_SELECT);
        }
        
#ifdef WIN32
        if(_kbhit())
#else
        if(pollsock(infd, POLLIN) == POLLIN)
#endif
        {
            string str;
            if(!std::getline(std::cin, str))
            {
                err(std::getline);
                goto killswitch;
            }
            
#ifdef WIN32
            while(_kbhit()) getchar();
#endif
            
            scr.eval(str);
        }
        
        if(!sock || !soc->avail()) goto nocoffei;
        
        ret = soc->readbuf();
        if(ret <= 0) { wsaerr(soc->readbuf); destroyconnect(); goto nocoffei; }
        
        do
        {
            u32 prevbps = SDL_GetTicks() - bpstick;
            bpsticks[bpscurrwrite++] = (float)ret / (float)prevbps * 1024.0F;
            bpstick = SDL_GetTicks();
            if(bpscurrwrite == BPSNO) bpscurrwrite = 0;
        }
        while(0);
        
        switch(p->packetid)
        {
            case 0x01: //ERROR
                printf("Disconnected by error (%i): ", p->data[0]);
                int i;
                for(i = 1; i != p->size; i++)
                    putchar(p->data[i]);
                putchar('\n');
                errno = 1;
                wsaerr(soc->readbuf);
                destroyconnect();
                goto nocoffei;
                break;
                
            case 0x02: //MODESET
                pdata = (u32*)p->data;
                
                printf("ModeTOP: %04X (o: %i, bytesize: %i)\n", pdata[0], pdata[0] & 7, pdata[1]);
                printf("ModeBOT: %04X (o: %i, bytesize: %i)\n", pdata[2], pdata[2] & 7, pdata[3]);
                
                if(pdata[1])
                {
                    srcfmt[0] = pdata[0];
                    stride[0] = pdata[1];
                }
                
                if(pdata[3])
                {
                    srcfmt[1] = pdata[2];
                    stride[1] = pdata[3];
                }
                
                bsiz[0] = stride[0] / 240;
                bsiz[1] = stride[1] / 240;
                
                SDL_FreeSurface(img[0]);
                SDL_FreeSurface(img[2]);
                SDL_FreeSurface(img[1]);
                
                img[0] = mksurface(stride[0] / bsiz[0], 400, bsiz[0], srcfmt[0]);
                img[2] = mksurface(stride[0] / bsiz[0], 400, bsiz[0], srcfmt[0]);
                img[1] = mksurface(stride[1] / bsiz[1], 320, bsiz[1], srcfmt[1]);
                break;
            
            case 0x03: //DATA_TGA
            {
                tga.image_data = decbuf;
                res = tga_read_from_FILE(&tga, p->data);
                if(res) { errtga(read_from_FILE); break; }
                
                u32 offs = tga.origin_y;
                offs = ((offs / 400) * (256 * 400 * 4)) + (stride[((offs / 400) & 1)] * (offs % 400));
                
                memcpy(sbuf + offs, decbuf, tga.height * stride[(tga.origin_y / 400) & 1]);
                
                if(!tga.origin_y)
                {
                    u32 prev = SDL_GetTicks() - fpstick;
                    fpsticks[fpscurrwrite++] = prev;
                    fpstick = SDL_GetTicks();
                    if(fpscurrwrite == FPSNO) fpscurrwrite = 0;
                }
                break;
            }
            
            case 0x04: //DATA_JPEG
            {
                int iw = 240;
                int ih = 1;
                int sus = 0;
                
                u32 offs = *(u32*)&p->data[0];
                offs = ((offs / 400) * (256 * 400 * 4)) + (stride[((offs / 400) & 1)] * (offs % 400));
                
                tjDecompressHeader2(jdec, &p->data[8], p->size - 8, &iw, &ih, &sus);
                tjDecompress2(jdec, &p->data[8], p->size - 8, sbuf + offs, iw, 0, ih, (srcfmt[(*(u32*)&p->data[0] / 400) & 1] & 1) ? TJPF_RGB : TJPF_RGBA, TJFLAG_FASTUPSAMPLE | TJFLAG_NOREALLOC | TJFLAG_FASTDCT);
                
                if(!*(u32*)&p->data[0])
                {
                    u32 prev = SDL_GetTicks() - fpstick;
                    fpsticks[fpscurrwrite++] = prev;
                    fpstick = SDL_GetTicks();
                    if(fpscurrwrite == FPSNO) fpscurrwrite = 0;
                }
                
                break;
            }
            
            case 0x7E: //CFGBLK_IN
                //TODO: configblk
                break;
            
            case 0xFF: //DEBUG
            {
                if(!p->size) break; //PING
                
                printf("DebugMSG (0x%X):", p->size);
                int i = 0;
                while(i < p->size)
                {
                    printf(" %08X", *(u32*)&p->data[i]);
                    i += 4;
                }
                putchar('\n');
                
                break;
            }
            
            default:
                printf("Unknown packet: %i\n", p->packetid);
                break;
        }
        
        scr.eval("Hz_PktIn();");
        
        
        nocoffei:
        
        for(int i = 0; i != 3; i++)
        if(img[i])
        {
            SDL_LockSurface(img[i]);
            memcpy(img[i]->pixels, sbuf + (256 * 400 * 4 * i), stride[i] * ((i & 1) ? 320 : 400));
            SDL_UnlockSurface(img[i]);
            
            SDL_DestroyTexture(tex[i]);
            tex[i] = SDL_CreateTextureFromSurface(rendertop, img[i]);
        }
        
        SDL_Point center;
        center.x = 0;
        center.y = 0;
        
        SDL_Rect soos;
        soos.x = 0;
        soos.y = 0;
        soos.w = 240;
        soos.h = 400;
        
        SDL_Rect dest;
        dest.x = 0;
        dest.y = 240;
        dest.w = 240;
        dest.h = 400;
        SDL_RenderCopyEx(rendertop, tex[0], &soos, &dest, 270.0F, &center, SDL_FLIP_NONE);
        
        soos.x = 0;
        soos.y = 0;
        soos.w = 240;
        soos.h = 320;
        
        dest.x = 400;
        dest.y = 240;
        dest.w = 240;
        dest.h = 320;
        SDL_RenderCopyEx(rendertop, tex[1], &soos, &dest, 270.0F, &center, SDL_FLIP_NONE);
        
        soos.x = 0;
        soos.y = 0;
        soos.w = 240;
        soos.h = 400;
        
        dest.x = 400;
        dest.y = 480;
        dest.w = 240;
        dest.h = 400;
        SDL_RenderCopyEx(rendertop, tex[2], &soos, &dest, 270.0F, &center, SDL_FLIP_NONE);
        
        
        snprintf(printbuf, sizeof(printbuf), "HzScreen " BUILDTIME "\n\nFPS: %.3f\nBPS: %.3f kb/s\nIMG: %i\nJPG: ", fps, bps / 1000.0F, scre);
        if(jpgq)
            sprintf(printbuf + strlen(printbuf), "%i%%", jpgq);
        else
            strcat(printbuf, "0% (Targa)");
        
        drawtext(printbuf, 408, 8, 0xFF7F00, 2, 3);
        //drawtext("ohaii :D\nmultiline test\nkek\n\neh", 16, 8, 0xFF);
        //drawtext("scaled text\nmultiline too!", 16, 80, 0xFF7F00, 2, 4);
        //drawtext(";)", 16, 144, 0xFF00, 5, 5);
        //drawtext("\x0B", 122, 144, 0xFF0000, 5, 5);
        //drawtext("\x0B", 122 + 16, 144 + 16, 0x0000FF, 5, 5);
        
        sprintf(printbuf, "X: %i\nY: %i\n\nX: %i\nY: %i", cpos.dx, cpos.dy, dpos.dx, dpos.dy);
        drawtext(printbuf, 0, 0, 0xFF7F00, 2, 3);
        
        /*if(kHeld & KEY_START)*/ scr.eval("Hz_Tick();");
        
        SDL_RenderPresent(rendertop);
        
        SDL_SetRenderDrawBlendMode(rendertop, SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(rendertop, 0, 0x7F, 0xFF, 16);
        SDL_RenderClear(rendertop);
        
        if(fpsoldwrite != fpscurrwrite)
        {
            float currfps = 0.0F;
            for(int i = 0; i != FPSNO; i++) currfps += fpsticks[i];
            currfps /= FPSNO;
            fps = 1000.0F / currfps;
            fpsoldwrite = fpscurrwrite;
        }
        
        if(bpsoldwrite != bpscurrwrite)
        {
            float currbps = 0.0F;
            for(int i = 0; i != BPSNO; i++) currbps += bpsticks[i];
            currbps /= BPSNO;
            bps = currbps;
            bpsoldwrite = bpscurrwrite;
        }
        
        if(DInput) continue;
        
        if(!soc) continue;
        
        if(kUp & KEY_SELECT)
        {
            puts("Sending JPEG quality cfgblk");
            p->packetid = 0x7E; //CFGBLK_IN
            p->size = 4 + 1;
            p->data[0] = 3; //JPEG_QUALITY
            p->data[4] = jpgq;
            if(soc->wribuf() <= 0) wsaerr(soc->wribuf);
            
            puts("Sending stream enable cfgblk");
            p->packetid = 0x7E; //CFGBLK_IN
            p->size = 4 + 1;
            p->data[0] = 0; //STREAM_ENABLE
            //p->data[4] = 1 | 2; //SCREEN_TOP | SCREEN_BOTTOM
            p->data[4] = 1; //SCREEN_TOP
            if(soc->wribuf() <= 0) wsaerr(soc->wribuf);
            
            puts("Setup done");
        }
        if(kDown & KEY_UP)
        {
            if(jpgq < 100)
            {
                jpgq++;
                
                p->packetid = 0x7E; //CFGBLK_IN
                p->size = 4 + 1;
                p->data[0] = 3; //JPEG_QUALITY
                p->data[4] = jpgq;
                if(soc->wribuf() <= 0) wsaerr(soc->wribuf);
            }
        }
        if(kDown & KEY_DOWN)
        {
            if(jpgq > 0)
            {
                jpgq--;
                
                p->packetid = 0x7E; //CFGBLK_IN
                p->size = 4 + 1;
                p->data[0] = 3; //JPEG_QUALITY
                p->data[4] = jpgq;
                if(soc->wribuf() <= 0) wsaerr(soc->wribuf);
            }
        }
        if(kDown & KEY_LEFT)
        {
            if(scre > 0)
            {
                scre--;
                
                if(scre > 3) scre = 3;
                
                p->packetid = 0x7E; //CFGBLK_IN
                p->size = 4 + 1;
                p->data[0] = 0; //STREAM_ENABLE
                p->data[4] = scre; //SCREEN_TOP
                if(soc->wribuf() <= 0) wsaerr(soc->wribuf);
                
                if(!scre) memset(sbuf, 0, sizeof(sbuf));
            }
        }
        if(kDown & KEY_RIGHT)
        {
            if(scre < 4)
            {
                scre++;
                
                if(scre == 4) scre++;
                
                p->packetid = 0x7E; //CFGBLK_IN
                p->size = 4 + 1;
                p->data[0] = 0; //STREAM_ENABLE
                p->data[4] = scre; //SCREEN_TOP
                if(soc->wribuf() <= 0) wsaerr(soc->wribuf);
            }
        }
    }
    
    killswitch:
    
    destroyconnect();
    
    deinittext();
    
#ifdef WIN32
    WSACleanup();
#endif
    
    return 0;
}
