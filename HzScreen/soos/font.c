#include <platform.h>

#include <SDL2/SDL.h>

#include "ctrufont_bin.h"

#include "font.h"

static SDL_Texture* font = 0;
static SDL_Renderer* rendertop = 0;

void inittext(SDL_Renderer* rt)
{
    rendertop = rt;
    if(font) SDL_DestroyTexture(font);
    font = SDL_CreateTexture(rendertop, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STATIC, 128, 128);
    if(!font)
    {
        printf("[FONT] SDL error: %s\n", SDL_GetError());
        return;
    }
    
    u32 _1[128 * 128];
    
    int i, j, k;
    for(i = 0; i != 0x100; i++)
        for(j = 0; j != 8; j++)
            for(k = 0; k != 8; k++)
                _1[((i >> 4) << 10) + ((i & 0xF) << 3) + (j << 7) + k] = (ctrufont_bin[(i << 3) + j] & (1 << (~k & 7))) ? -1U : 0;
    
    if(SDL_UpdateTexture(font, 0, _1, 128 * 4))
    {
        printf("[FONT] SDL error: %s\n", SDL_GetError());
        return;
    }
    
    SDL_SetTextureBlendMode(font, SDL_BLENDMODE_BLEND);
}

void deinittext()
{
    if(font) SDL_DestroyTexture(font);
    font = 0;
}

void rendertext(const char* wat, int sx, int sy, int esx, int esy, int len, int color, int scx, int scy)
{
    if(!font) return;
    
    if(color >> 24) SDL_SetTextureAlphaMod(font, color >> 24); else SDL_SetTextureAlphaMod(font, 0xFF);
    SDL_SetTextureColorMod(font, (color & 0xFF), (color >> 8) & 0xFF, (color >> 16) & 0xFF);
    
    SDL_Rect dest;
    dest.w = scx * 8;
    dest.h = scy * 8;
    
    SDL_Rect src;
    src.w = 8;
    src.h = 8;
    
    //int i = 0;
    int x = sx;
    int y = sy;
    char c;
    
    int ex = 0;
    int ey = 0;
    SDL_GetRendererOutputSize(rendertop, &ex, &ey);
    
    if(esx < ex) ex = esx;
    if(esy < ey) ey = esy;
    
    ex -= dest.w;
    ey -= dest.h;
    
    while(y <= ey)
    {
        while(x <= ex)
        {
            c = *(wat++);
            
            if(!len--) return;
            if(!c && len < 0) return;
            if(c == '\n') break;
            
            dest.x = x;
            dest.y = y;
            src.x = (c & 0xF) << 3;
            src.y = ((c >> 4) & 0xF) << 3;
            
            SDL_RenderCopyEx(rendertop, font, &src, &dest, 0.0, 0, SDL_FLIP_NONE);
            
            x += dest.w;
        }
        
        x = sx;
        y += dest.h;
    }
}
