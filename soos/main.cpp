#include <3ds.h>

/*
    HorizonM - utility background process for the Horizon operating system
    Copyright (C) 2017 MarcusD (https://github.com/MarcuzD)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

extern "C"
{
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <setjmp.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <malloc.h>
#include <errno.h>
#include <stdarg.h>
#include <fcntl.h>
#include <sys/iosupport.h>

#include <poll.h>
#include <arpa/inet.h>

#include "miscdef.h"
#include "service/screen.h"
#include "service/mcu.h"
#include "misc/pattern.h"

#include "tga/targa.h"
#include <turbojpeg.h>
}

#include <exception>

#include "utils.hpp"



#define YieldIdle() svcSleepThread(2e8)

#define hangmacro()\
{\
    memset(&pat.r[0], 0x7F, 16);\
    memset(&pat.g[0], 0x7F, 16);\
    memset(&pat.b[0], 0x00, 16);\
    memset(&pat.r[16], 0, 16);\
    memset(&pat.g[16], 0, 16);\
    memset(&pat.b[16], 0, 16);\
    pat.ani = 0x1006;\
    PatApply();\
    while(1)\
    {\
        hidScanInput();\
        if(hidKeysHeld() == (KEY_SELECT | KEY_START))\
        {\
            goto killswitch;\
        }\
        YieldIdle();\
    }\
}

static int haznet = 0;
int checkwifi()
{
    haznet = 0;
    u32 wifi = 0;
    hidScanInput();
    if(hidKeysHeld() == (KEY_SELECT | KEY_START)) return 0;
    if(ACU_GetWifiStatus(&wifi) >= 0 && wifi) haznet = 1;
    return haznet;
}


int pollsock(int sock, int wat, int timeout = 0)
{
    struct pollfd pd;
    pd.fd = sock;
    pd.events = wat;
    
    if(poll(&pd, 1, timeout) == 1)
        return pd.revents & wat;
    return 0;
}

class bufsoc
{
public:
    
    typedef struct
    {
        u32 packetid : 8;
        u32 size : 24;
        u8 data[0];
    } packet;
    
    int sock;
    u8* buf;
    int bufsize;
    int recvsize;
    
    bufsoc(int sock, int bufsize)
    {
        this->bufsize = bufsize;
        buf = new u8[bufsize];
        
        recvsize = 0;
        this->sock = sock;
    }
    
    ~bufsoc()
    {
        if(!this) return;
        close(sock);
        delete[] buf;
    }
    
    int avail()
    {
        return pollsock(sock, POLLIN) == POLLIN;
    }
    
    int readbuf(int flags = 0)
    {
        u32 hdr = 0;
        int ret = recv(sock, &hdr, 4, flags);
        if(ret < 0) return -errno;
        if(ret < 4) return -1;
        *(u32*)buf = hdr;
        
        packet* p = pack();
        
        int mustwri = p->size;
        int offs = 4;
        while(mustwri)
        {
            ret = recv(sock, buf + offs , mustwri, flags);
            if(ret <= 0) return -errno;
            mustwri -= ret;
            offs += ret;
        }
        
        recvsize = offs;
        return offs;
    }
    
    int wribuf_old(int flags = 0)
    {
        int mustwri = pack()->size + 4;
        int offs = 0;
        int ret = 0;
        while(mustwri)
        {
            ret = send(sock, buf + offs , mustwri, flags);
            if(ret < 0) return -errno;
            mustwri -= ret;
            offs += ret;
        }
        
        return offs;
    }
    
    int wribuf(int flags = 0)
    {
        int mustwri = pack()->size + 4;
        int offs = 0;
        int ret = 0;
        
        while(mustwri)
        {
            if(mustwri >> 12)
                ret = send(sock, buf + offs , 0x1000, flags);
            else
                ret = send(sock, buf + offs , mustwri, flags);
            if(ret < 0) return -errno;
            mustwri -= ret;
            offs += ret;
        }
        
        return offs;
    }
    
    packet* pack()
    {
        return (packet*)buf;
    }
    
    int errformat(char* c, ...)
    {
        packet* p = pack();
        
        int len = 0;
        
        va_list args;
        va_start(args, c);
        len = vsprintf((char*)p->data + 1, c, args);
        va_end(args);
        
        if(len < 0)
        {
            puts("errformat: out of memory"); //???
            return -1;
        }
        
        printf("Packet error 0x%02X: %s\n", p->packetid, p->data + 1);
        
        p->data[0] = p->packetid;
        p->packetid = 1;
        p->size = len + 2;
        
        return wribuf();
    }
};

static jmp_buf __exc;
static int  __excno;

void CPPCrashHandler()
{
    puts("\e[0m\n\n- The application has crashed\n\n");
    
    try
    {
        throw;
    }
    catch(std::exception &e)
    {
        printf("std::exception: %s\n", e.what());
    }
    catch(Result res)
    {
        printf("Result: %08X\n", res);
        //NNERR(res);
    }
    catch(int e)
    {
        printf("(int) %i\n", e);
    }
    catch(...)
    {
        puts("<unknown exception>");
    }
    
    puts("\n");
    
    PatStay(0xFFFFFF);
    PatPulse(0xFF);
    
    svcSleepThread(1e9);
    
    hangmacro();
    
    killswitch:
    longjmp(__exc, 1);
}

Result syncdma(Handle desth, u32 dest, Handle srch, void* src, u32 size)
{
    Handle dmahand = 0;
    
    u8 dmaconf[0x18];
    memset(dmaconf, 0, sizeof(dmaconf));
    dmaconf[0] = -1; //don't care
    
    Result res = svcFlushProcessDataCache(desth, (void*)dest, size);
    if(res < 0) return res;
    
    res = svcFlushProcessDataCache(srch, src, size);
    if(res < 0) return res;
    
    res = svcStartInterProcessDma(&dmahand, desth, (void*)dest, srch, src, size, dmaconf);
    if(res < 0) return res;
    
    u32 stat;
    while(1)
    {
        svcGetDmaState(&stat, dmahand);
        if(stat & 0xFFF00000)
            break;
        else
            svcSleepThread(20000);
    }
    
    svcStopDma(dmahand);
    svcCloseHandle(dmahand);
    
    svcInvalidateProcessDataCache(desth, (void*)dest, size);
    
    return 0;
}


extern "C" u32 __get_bytes_per_pixel(GSPGPU_FramebufferFormats format);

const int port = 6464;

static u32 kDown = 0;
static u32 kHeld = 0;
static u32 kUp = 0;

static int isold = 1;

static Result ret = 0;
static int cx = 0;
static int cy = 0;

static u8 cfgblk[0x100];

static int sock = 0;

static struct sockaddr_in sai;
static socklen_t sizeof_sai = sizeof(sai);

static bufsoc* soc = nullptr;

static bufsoc::packet* k = nullptr;

static Thread netthread = 0;
static vu32 threadrunning = 0;

static tga_image img;
static tjhandle jencode = nullptr;


void netfunc(void* __dummy_arg__)
{
    Result res = 0;
    Handle memhand = 0;
    
    if(isold);// screenbuf = (u32*)k->data;
    else osSetSpeedupEnable(1);
    
    k = soc->pack(); //Just In Case (tm)
    
    PatStay(0xFF00);
    
    u32 stride[2] = {400, 240};
    u32 format[2] = {0xF00FCACE, 0xF00FCACE};
    
    PatPulse(0x7F007F);
    threadrunning = 1;
    
    /*do
    {
        k->packetid = 2; //MODE
        k->size = 4 * 4;
        
        u32* kdata = (u32*)k->data;
        
        kdata[0] = 1;
        kdata[1] = 240 * 3;
        kdata[2] = 1;
        kdata[3] = 240 * 3;
        soc->wribuf();
    }
    while(0);*/
    
    while(threadrunning)
    {
        if(soc->avail())
        while(1)
        {
            if((kHeld & (KEY_SELECT | KEY_START)) == (KEY_SELECT | KEY_START))
            {
                delete soc;
                soc = nullptr;
                break;
            }
            
            //puts("reading");
            cy = soc->readbuf();
            if(cy <= 0)
            {
                printf("Failed to recvbuf: (%i) %s\n", errno, strerror(errno));
                delete soc;
                soc = nullptr;
                break;
            }
            else
            {
                //printf("#%02X 0x%X\n", k->packetid, k->size);
                
                reread:
                switch(k->packetid)
                {
                    case 0x00: //CONNECT
                    case 0x01: //ERROR
                        puts("forced dc");
                        delete soc;
                        soc = nullptr;
                        break;
                        
                    case 0x7E: //CFGBLK_IN
                        memcpy(cfgblk + k->data[0], &k->data[4], min((u32)(0x100 - k->data[0]), (u32)(k->size - 4)));
                        break;
                    
                    case 0x80: //DMA_REQ
                        soc->errformat("NYI");
                        break;
                    
                    case 0x81: //DMA_IN
                    {
                        u32 pid = *(u32*)&k->data[0];
                        u32 ploc = *(u32*)&k->data[4];
                        
                        if(k->size <= 8)
                        {
                            soc->errformat("Invalid packet size 0x%X for DMA_IN", k->size);
                            break;
                        }
                        
                        if(pid == -1U) svcGetProcessId(&pid, 0xFFFF8001); //why would you want to do this?!
                        
                        res = svcOpenProcess(&memhand, pid);
                        if(res < 0)
                        {
                            soc->errformat("Failed to open process 0x%X: %08X", res);
                            break;
                        }
                        
                        res = svcControlProcessMemory(memhand, ploc & 0xFFFFF000, ploc & 0xFFFFF000, (k->size - 8 + 0xFFF) & 0xFFFFF000, MEMOP_PROT, MEMPERM_READ | MEMPERM_WRITE | MEMPERM_EXECUTE);
                        if(res < 0)
                        {
                            svcCloseHandle(memhand);
                            soc->errformat("Failed to change memory perms for address 0x%08X: %08X", ploc, res);
                            break;
                        }
                        
                        res = syncdma(memhand, ploc, 0xFFFF8001, k->data + 8, k->size - 8);
                        svcCloseHandle(memhand);
                        if(res < 0)
                        {
                            soc->errformat("Failed to DMA to address 0x%08X: %08X", ploc, res);
                            break;
                        }
                        
                        break;
                    }
                        
                    default:
                        printf("Invalid packet ID: %i\n", k->packetid);
                        delete soc;
                        soc = nullptr;
                        break;
                }
                
                break;
            }
        }
        
        if(!soc) break;
        
        if(cfgblk[0])
        {
            //test for changed framebuffers
            if\
            (\
                SCREEN_FMT_TOP != format[0]\
                ||\
                SCREEN_FMT_BOT != format[1]\
            )
            {
                PatStay(0xFFFF00);
                
                format[0] = SCREEN_FMT_TOP;
                format[1] = SCREEN_FMT_BOT;
                
                k->packetid = 2; //MODE
                k->size = 4 * 4;
                
                u32* kdata = (u32*)k->data;
                
                kdata[0] = format[0];
                kdata[1] = SCREEN_STR_TOP;
                kdata[2] = format[1];
                kdata[3] = SCREEN_STR_BOT;
                soc->wribuf();
                
                PatStay(0xFF00);
            }
            
            int rscr = 0;
            int scr;
            if(cfgblk[0] & 1)
            do
            {
                scr = rscr & 1;
                
                if(format[scr] == 0xF00FCACE)
                {
                    printf("Screen #%i is F00FCACE\n", scr);
                    continue;
                }
                
                u8* screenbuf = nullptr;
                
                do
                {
                    if(rscr & 1)
                    {
                        if(SCREEN_SEL_BOT & 1)
                            screenbuf = (u8*)SCREEN_FB1_BOT;
                        else
                            screenbuf = (u8*)SCREEN_FB0_BOT;
                    }
                    else
                    {
                        if(SCREEN_SEL_TOP & 1)
                        {
                            if((rscr >> 1) & 1)
                                screenbuf = (u8*)SCREEN_FB3_TOP;
                            else
                                screenbuf = (u8*)SCREEN_FB1_TOP;
                        }
                        else
                        {
                            if((rscr >> 1) & 1)
                                screenbuf = (u8*)SCREEN_FB2_TOP;
                            else
                                screenbuf = (u8*)SCREEN_FB0_TOP;
                        }
                    }
                }
                while(0);
                
                if(screenbuf > (u8*)0x28000000 && screenbuf < (u8*)0x2FF9C000);
                else if(screenbuf > (u8*)0x20000000 && screenbuf < (u8*)0x27F9C000);
                else if(screenbuf > (u8*)0x18000000 && screenbuf < (u8*)0x1859C000);
                else
                {
                    printf("Screen #%i invalid PA: 0x%08X\n", rscr, screenbuf);
                    continue;
                }
                
                screenbuf = (u8*)(((u32)screenbuf) | 0x80000000);
                
                u32 fbstride[2] = {SCREEN_STR_TOP, SCREEN_STR_BOT};
                
                u32 siz = (fbstride[scr] * stride[scr]);
                
                u32 bsiz = fbstride[scr] / 240;
                u32 scrw = fbstride[scr] / bsiz;
                u32 bits = 4 << bsiz;
                
                if((format[scr] & 7) == 2) bits = 17;
                if((format[scr] & 7) == 4) bits = 18;
                
                k->size = 0;
                
                int imgsize = soc->bufsize;
                
                if((format[scr] & 7) >> 1 || !cfgblk[3])
                {
                    init_tga_image(&img, (u8*)screenbuf, scrw, stride[scr], bits);
                    img.image_type = TGA_IMAGE_TYPE_BGR_RLE;
                    //img.origin_y = (rscr * 400) + (stride[scr] * offs[rscr]);
                    img.origin_y = (rscr * 400);// + stride[scr];
                    tga_write_to_FILE(k->data, &img, &imgsize);
                    
                    k->packetid = 3; //DATA (Targa)
                    k->size = imgsize;
                }
                else
                {
                    *(u32*)&k->data[0] = (rscr * 400);// + stride[scr];
                    u8* dstptr = &k->data[8];
                    if(tjCompress2(jencode, (u8*)screenbuf, scrw, bsiz * scrw, stride[scr], (format[scr] & 7) ? TJPF_RGB : TJPF_XBGR, &dstptr, (u32*)&imgsize, TJSAMP_420, cfgblk[3], TJFLAG_NOREALLOC | TJFLAG_FASTDCT))
                    {
                        //puts(tjGetErrorStr());
                        //soc->errformat(tjGetErrorStr());
                    }
                    else
                    {
                        k->size = imgsize + 8;
                        k->packetid = 4; //DATA (JPEG)
                    }
                }
                
                if(k->size) soc->wribuf();
                
                if(isold) svcSleepThread(2e6);
            }
            while(0);
        }
        else
        {
            if(cfgblk[0xFF])
            {
                k->packetid = 0xFF;
                k->size = 0;
                soc->wribuf();
                if(cfgblk[0xFF] == 0xFF)
                    gspWaitForVBlank();
                else
                    svcSleepThread(1e6 * cfgblk[0xFF]);
            }
        }
    }
    
    memset(&pat.r[0], 0xFF, 16);
    memset(&pat.g[0], 0xFF, 16);
    memset(&pat.b[0], 0x00, 16);
    memset(&pat.r[16],0x7F, 16);
    memset(&pat.g[16],0x00, 16);
    memset(&pat.b[16],0x7F, 16);
    pat.ani = 0x0406;
    PatApply();
    
    if(soc)
    {
        puts("Killing networking");
        delete soc;
        soc = nullptr;
    }
    
    puts("Thread stopping...");
    threadrunning = 0;
}

static FILE* f = nullptr;

ssize_t stdout_write(struct _reent* r, int fd, const char* ptr, size_t len)
{
    if(!f) return 0;
    int ret = fwrite(ptr, 1, len, f);
    if(ret > 0) fflush(f);
    return ret;
}

static const devoptab_t devop_stdout = { "stdout", 0, nullptr, nullptr, stdout_write, nullptr, nullptr, nullptr };

int main()
{
    mcuInit();
    nsInit();
    
    soc = nullptr;
    
    f = fopen("/HzLog.log", "w");
    if(f <= 0) f = nullptr;
    else
    {
        devoptab_list[STD_OUT] = &devop_stdout;
        devoptab_list[STD_ERR] = &devop_stdout;
        
        setvbuf(stdout, nullptr, _IONBF, 0);
        setvbuf(stderr, nullptr, _IONBF, 0);
    }
    
    memset(&pat, 0, sizeof(pat));
    //memset(&capin, 0, sizeof(capin));
    memset(cfgblk, 0, sizeof(cfgblk));
    
    isold = APPMEMTYPE <= 5;
    
    printf("APPMEMTYPE is %i, so we must be on %s\n", APPMEMTYPE, isold ? "an old3DS" : "a new3DS");
    
    
    /*if(isold)
    {
        limit[0] = 8;
        limit[1] = 8;
        stride[0] = 50;
        stride[1] = 40;
    }
    else
    {
        limit[0] = 1;
        limit[1] = 1;
        stride[0] = 400;
        stride[1] = 320;
    }*/
    
    
    PatStay(0xFF);
    
    acInit();
    
    do
    {
        //u32 siz = isold ? 0x10000 : 0x200000;
        u32 siz = 0x10000;
        ret = socInit((u32*)memalign(0x1000, siz), siz);
    }
    while(0);
    if(ret < 0) *(u32*)0x1000F0 = ret;//hangmacro();
    
    jencode = tjInitCompress();
    if(!jencode) *(u32*)0x1000F0 = 0xDEADDEAD;//hangmacro();
    
    gspInit();
    
    //gxInit();
    
    /*if(isold)
        screenbuf = (u32*)memalign(8, 50 * 240 * 4);
    else
        screenbuf = (u32*)memalign(8, 400 * 240 * 4);
    
    if(!screenbuf)
    {
        makerave();
        svcSleepThread(2e9);
        hangmacro();
    }*/
    
    
    if((__excno = setjmp(__exc))) goto killswitch;
      
#ifdef _3DS
    std::set_unexpected(CPPCrashHandler);
    std::set_terminate(CPPCrashHandler);
#endif
    
    netreset:
    
    if(sock)
    {
        close(sock);
        sock = 0;
    }
    
    if(haznet && errno == EINVAL)
    {
        puts("Waiting for wifi to die");
        errno = 0;
        PatStay(0xFFFF);
        while(checkwifi()) YieldIdle();
    }
    
    if(checkwifi())
    {
        cy = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
        if(cy <= 0)
        {
            printf("socket error: (%i) %s\n", errno, strerror(errno));
            hangmacro();
        }
        
        sock = cy;
        
        struct sockaddr_in sao;
        sao.sin_family = AF_INET;
        sao.sin_addr.s_addr = gethostid();
        sao.sin_port = htons(port);
        
        if(bind(sock, (struct sockaddr*)&sao, sizeof(sao)) < 0)
        {
            printf("bind error: (%i) %s\n", errno, strerror(errno));
            hangmacro();
        }
        
        //fcntl(sock, F_SETFL, fcntl(sock, F_GETFL, 0) | O_NONBLOCK);
        
        if(listen(sock, 1) < 0)
        {
            printf("listen error: (%i) %s\n", errno, strerror(errno));
            hangmacro();
        }
    }
    
    
    reloop:
    
    if(!isold) osSetSpeedupEnable(1);
    
    PatPulse(0xFF40FF);
    if(haznet) PatStay(0xCCFF00);
    else PatStay(0xFFFF);
    
    while(1)
    {
        hidScanInput();
        kDown = hidKeysDown();
        kHeld = hidKeysHeld();
        kUp = hidKeysUp();
        
        //printf("svcGetSystemTick: %016llX\n", svcGetSystemTick());
        
        if(kDown) PatPulse(0xFF);
        if(kHeld == (KEY_SELECT | KEY_START)) break;
        
        if(!soc)
        {
            if(!haznet)
            {
                if(checkwifi()) goto netreset;
            }
            else if(netthread)
            {
                puts("Waiting for network thread to die");
                
                if(threadJoin(netthread, 3e9) < 0)
                {
                    puts("Killing thread forcefully");
                    threadFree(netthread);
                    
                    threadrunning = 0;
                }
                
                netthread = nullptr;
                
                puts("Relooping");
                
                goto reloop;
            }
            else if(pollsock(sock, POLLIN) == POLLIN)
            {
                sizeof_sai = sizeof(sai);
                int cli = accept(sock, (struct sockaddr*)&sai, &sizeof_sai);
                if(cli < 0)
                {
                    printf("Failed to accept client: (%i) %s\n", errno, strerror(errno));
                    if(errno == EINVAL) goto netreset;
                    PatPulse(0xFF);
                }
                else
                {
                    PatPulse(0xFF00);
                    //soc = new bufsoc(cli, isold ? 0xC000 : 0x70000);
                    soc = new bufsoc(cli, 0x8000);
                    k = soc->pack();
                    
                    if(isold)
                    {
                        netthread = threadCreate(netfunc, nullptr, 0x1800, 0x14, 1, true);
                    }
                    else
                    {
                        netthread = threadCreate(netfunc, nullptr, 0x4000, 10, 3, true);
                    }
                    
                    if(!netthread)
                    {
                        memset(&pat, 0, sizeof(pat));
                        memset(&pat.r[0], 0xFF, 16);
                        pat.ani = 0x102;
                        PatApply();
                        
                        svcSleepThread(2e9);
                    }
                    
                    
                    if(netthread)
                    {
                        puts("Waiting for netthread to start");
                        while(!threadrunning) YieldIdle();
                        puts("Started");
                    }
                    else
                    {
                        delete soc;
                        soc = nullptr;
                        hangmacro();
                    }
                }
            }
            else if(pollsock(sock, POLLERR) == POLLERR)
            {
                printf("POLLERR (%i) %s", errno, strerror(errno));
                goto netreset;
            }
        }
        
        if(netthread && !threadrunning)
        {
            puts("Fixing up after thread death");
            if(soc)
            {
                puts("Killing orphaned socket connection");
                delete soc;
                soc = nullptr;
            }
            
            //TODO todo?
            netthread = nullptr;
            goto reloop;
        }
        
        if((kHeld & (KEY_ZL | KEY_ZR)) == (KEY_ZL | KEY_ZR))
        {
            u32* ptr = (u32*)0x1F000000;
            int o = 0x00600000 >> 2;
            while(o--) *(ptr++) = rand();
        }
        
        YieldIdle();
    }
    
    killswitch:
    
    PatStay(0xFF0000);
    
    if(netthread)
    {
        threadrunning = 0;
        
        puts("Waiting for thread kill");
        
        if(threadJoin(netthread, 3e9) < 0) threadFree(netthread);
        
        puts("assuming thread-kill");
    }
    
    if(soc) delete soc;
    else close(sock);
    
    puts("Shutting down sockets...");
    SOCU_ShutdownSockets();
    
    socExit();
    
    //gxExit();
    
    gspExit();
    
    acExit();
    
    if(f)
    {
        fflush(f);
        fclose(f);
    }
    
    PatStay(0);
    
    nsExit();
    
    mcuExit();
    
    return 0;
}
